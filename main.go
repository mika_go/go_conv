package main

import (
	"flag"
	"fmt"
	"gitlab.com/mika_go/go_conv/pkg/cli"
	"gitlab.com/mika_go/go_conv/pkg/modify"
	"gitlab.com/mika_go/go_conv/pkg/scan"
	"log"
	"os"
)

func main() {
	if err := cli.CheckFlags(); err != nil {
		fmt.Printf("Flags: %s\n", err.Error())
		flag.Usage()
		return
	}

	homePath, err := os.Getwd()
	if err != nil {
		fmt.Printf("OS: %s\n", err.Error())
		return
	}

	if path, err := cli.GetPath(); err != nil {
		fmt.Printf("Args: %s\n", err.Error())
		return
	} else {
		if err := os.Chdir(path); err != nil {
			log.Fatalf("OS: %s\n", err.Error())
			return
		}
	}

	scan.MakePackageMap()
	scan.ProjPath, err = scan.ProjectRelPath()
	if err != nil {
		fmt.Printf("Path: %s\n", err.Error())
		return
	}

	switch cli.GetType() {
	case cli.LOCAL:
		modify.ConvertToLocal()
	case cli.GLOBAL:
		modify.ConvertToGlobal()
	}

	if err := os.Chdir(homePath); err != nil {
		log.Fatalf("OS: %s\n", err.Error())
		return
	}
}

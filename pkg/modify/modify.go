package modify

import (
	"fmt"
	"gitlab.com/mika_go/go_conv/pkg/scan"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
)

func ConvertToLocal() {
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) != ".go" {
			return nil
		}

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		rows := strings.Split(string(data), "\n")
		isImport := false
		for i, row := range rows {
			if strings.Contains(row, "import") {
				isImport = true
			} else if strings.Contains(row, ")") {
				break
			}

			if !isImport {
				continue
			}

			re, err := regexp.Compile(`".*"`)
			if err != nil {
				return err
			}
			row = re.FindString(row)
			row = strings.Trim(row, `"`)

			if !scan.SearchPackage(row) {
				continue
			}

			rel, err := filepath.Rel(filepath.Dir(path), strings.TrimPrefix(row, scan.ProjPath+string(filepath.Separator)))
			if err != nil {
				return err
			}

			if !strings.HasPrefix(rel, "..") {
				rel = "." + string(filepath.Separator) + rel
			}

			rows[i] = re.ReplaceAllString(rows[i], fmt.Sprintf(`"%s"`, rel))
		}

		if isImport {
			if err := ioutil.WriteFile(path, []byte(strings.Join(rows, "\n")), 0644); err != nil {
				return err
			}
			if err := exec.Command("go", "fmt", path).Run(); err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		fmt.Printf("Convert walk: %s\n", err.Error())
		return
	}
}

func ConvertToGlobal() {
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) != ".go" {
			return nil
		}

		dir := filepath.Dir(path)

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		rows := strings.Split(string(data), "\n")
		isImport := false
		for i, row := range rows {
			if strings.Contains(row, "import") {
				isImport = true
			} else if strings.Contains(row, ")") {
				break
			}

			if !isImport {
				continue
			}

			re, err := regexp.Compile(`".*"`)
			if err != nil {
				return err
			}

			row = re.FindString(row)
			row = strings.Trim(row, `"`)

			paths := strings.Split(dir+string(filepath.Separator)+row, string(filepath.Separator))

			if len(paths) == 0 {
				continue
			}

			row = filepath.Join(paths...)

			ok, packRow := scan.SearchRelPackage(row)
			if !ok {
				continue
			}

			rows[i] = re.ReplaceAllString(rows[i], fmt.Sprintf(`"%s"`, packRow))
		}

		if isImport {
			if err := ioutil.WriteFile(path, []byte(strings.Join(rows, "\n")), 0644); err != nil {
				return err
			}
			if err := exec.Command("go", "fmt", path).Run(); err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		fmt.Printf("Convert walk: %s\n", err.Error())
		return
	}
}

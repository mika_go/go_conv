package cli

import (
	"errors"
	"flag"
)

const (
	LOCAL  = iota
	GLOBAL = iota
)

var (
	toLocal  *bool
	toGlobal *bool
)

var (
	ErrTwoConvertFlags = errors.New("only one convert flag must be used")
	ErrNoConvertFlags  = errors.New("you should choose one convert flag")
)

func init() {
	toLocal = flag.Bool("l", false, "Use flag if need convert imports to local")
	toGlobal = flag.Bool("g", false, "Use flag if need convert imports to global(relatively from $GOPATH)")
	flag.Parse()
}

func CheckFlags() error {
	if *toLocal && *toGlobal {
		return ErrTwoConvertFlags
	}
	if !*toLocal && !*toGlobal {
		return ErrNoConvertFlags
	}

	return nil
}

func GetType() int {
	if *toLocal {
		return LOCAL
	} else if *toGlobal {
		return GLOBAL
	}

	return -1
}

package cli

import (
	"errors"
	"flag"
	"os"
)

var (
	ErrManyArgs       = errors.New("args count must be 1 or 0")
	ErrTargetNotExist = errors.New("target directory doesn't exist")
)

func GetPath() (string, error) {
	args := flag.Args()
	if len(args) > 1 {
		return "", ErrManyArgs
	}

	if len(args) == 0 {
		return ".", nil
	}

	target := args[0]

	if _, err := os.Stat(target); os.IsNotExist(err) {
		return "", ErrTargetNotExist
	}

	return target, nil
}

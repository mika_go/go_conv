package scan

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var Packages []string
var ProjPath string

func MakePackageMap() {
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if path[0] == '.' || !info.IsDir() {
			return nil
		}

		lsRes, err := exec.Command("ls", path).Output()
		if err != nil {
			return err
		}

		files := strings.Split(strings.TrimSpace(string(lsRes)), "\n")
		for _, file := range files {
			if strings.HasSuffix(file, ".go") {
				abs, err := filepath.Abs(path)
				if err != nil {
					return err
				}
				rel, err := filepath.Rel(filepath.Join(os.Getenv("GOPATH"), "src"), abs)
				if err != nil {
					return err
				}

				Packages = append(Packages, rel)
				break
			}
		}
		return nil
	})
	if err != nil {
		fmt.Printf("Walk: %s\n", err.Error())
		return
	}
}

func SearchPackage(pack string) bool {
	if pack == "" {
		return false
	}
	for _, el := range Packages {
		if el == pack {
			return true
		}
	}

	return false
}

func SearchRelPackage(pack string) (bool, string) {
	if pack == "" {
		return false, ""
	}
	for i, el := range Packages {
		if strings.HasSuffix(el, pack) {
			return true, Packages[i]
		}
	}

	return false, ""
}

func ProjectRelPath() (string, error) {
	path, err := os.Getwd()
	if err != nil {
		return "", err
	}
	gosrc := filepath.Join(os.Getenv("GOPATH"), "src")

	return filepath.Rel(gosrc, path)
}
